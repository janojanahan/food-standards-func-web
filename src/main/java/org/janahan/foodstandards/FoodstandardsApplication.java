package org.janahan.foodstandards;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.janahan.foodstandards.service.AuthoritiesRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.netty.tcp.TcpClient;

import java.util.concurrent.TimeUnit;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.TEXT_HTML;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@SpringBootApplication
public class FoodstandardsApplication {
    private static final Integer REMOTE_SERVER_TIMEOUT_MILLIS = 10000;

    public static void main(String[] args) {
        SpringApplication.run(FoodstandardsApplication.class, args);
    }

    @Bean
    TcpClient tcpClient() {
        return TcpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, REMOTE_SERVER_TIMEOUT_MILLIS)
                .doOnConnected(connection ->
                        connection.addHandlerLast(new ReadTimeoutHandler(REMOTE_SERVER_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS))
                                .addHandlerLast(new WriteTimeoutHandler(REMOTE_SERVER_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                                ));
    }

    @Bean
    public RouterFunction<ServerResponse> routes(AuthoritiesRepository authoritiesRepository,
                                                 RatingsHandler ratingsHandler) {
        // Using inline handler for authorities, as they are relatively simple.

        var apiRoute = route()
                .nest(accept(APPLICATION_JSON), builder -> builder
                        .GET("/authority", request -> authoritiesRepository.findAllAuthorities()
                                .flatMap(authorities -> ServerResponse.ok()
                                        .contentType(APPLICATION_JSON)
                                        .body(fromObject(authorities))))
                        .GET("/authority/{id}/ratings", ratingsHandler::handleRatings))
                .build();

        return route().path("/api/", () -> apiRoute).build();
    }

    @Bean
    RouterFunction<ServerResponse> staticResourceRouter() {
        return RouterFunctions.resources("/**", new ClassPathResource("static/"));
    }

    //Gah do we really have to do this!!!!!!!
    @Bean
    public RouterFunction<ServerResponse> indexRouter(@Value("classpath:/static/index.html") final Resource indexHtml) {
        return route().GET("/", request -> ServerResponse.ok()
                .contentType(TEXT_HTML)
                .syncBody(indexHtml)).build();
    }

}
