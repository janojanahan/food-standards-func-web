package org.janahan.foodstandards.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Optional;

public class Authority {


    private final long idCode;
    private final String name;

    @JsonIgnore
    private final String region;

    public Authority(long idCode, String name, String region) {
        this.idCode = idCode;
        this.name = name;
        this.region = region;
    }

    public long getIdCode() {
        return idCode;
    }

    public String getName() {
        return name;
    }

    public Optional<String> getRegion() {
        return Optional.ofNullable(region);
    }

    public boolean isScotland() {
        return getRegion().map(x -> x.equalsIgnoreCase("Scotland")).orElse(false);
    }
}
