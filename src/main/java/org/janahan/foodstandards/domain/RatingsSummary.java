package org.janahan.foodstandards.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

public class RatingsSummary {

    @JsonProperty("ratings")
    private final List<RatingItem> ratingsList;

    public RatingsSummary(List<RatingItem> ratingsList) {
        this.ratingsList = Collections.unmodifiableList(ratingsList);
    }

    public List<RatingItem> getRatingsList() {
        return ratingsList;
    }

    @JsonProperty("totalEstablishments")
    public long getTotalEstablishments() {
        return ratingsList.stream()
                .mapToLong(RatingItem::getCount)
                .sum();
    }

    public static class RatingItem {

        private final HygeneRatingLevel hygeneRatingLevel;
        private final long count;

        public RatingItem(HygeneRatingLevel hygeneRatingLevel, long count) {

            this.hygeneRatingLevel = hygeneRatingLevel;
            this.count = count;
        }

        @JsonProperty("ratingLevel")
        public HygeneRatingLevel getHygeneRatingLevel() {
            return hygeneRatingLevel;
        }

        @JsonProperty("establishmentCount")
        public long getCount() {
            return count;
        }
    }
}
