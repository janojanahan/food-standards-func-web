package org.janahan.foodstandards;

import org.janahan.foodstandards.service.RatingsRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;

@Component
public class RatingsHandler {

    private final RatingsRepository ratingsRepository;

    public RatingsHandler(RatingsRepository ratingsRepository) {
        this.ratingsRepository = ratingsRepository;
    }

    Mono<ServerResponse> handleRatings(ServerRequest serverRequest) {
        try {
            long id = Long.parseLong(serverRequest.pathVariable("id"));
            return ratingsRepository.ratingsSummaryForAuthorityAsync(id)
                    .flatMap(ratings -> ServerResponse.ok().contentType(APPLICATION_JSON).body(fromObject(ratings)));
        } catch (NumberFormatException nfe) {
            return Mono.defer(() -> ServerResponse.badRequest().build());
        }
    }
}
