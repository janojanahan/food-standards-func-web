package org.janahan.foodstandards.service;

import org.janahan.foodstandards.domain.Authority;
import org.janahan.foodstandards.domain.HygeneRatingLevel;
import org.janahan.foodstandards.service.FsaAuthoritiesDto.FsaAuthorityDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.janahan.foodstandards.service.FsaEstablishmentsDto.FsaEstablishmentDto;

@Component
public class FsaServiceClient implements AuthoritiesRepository {
    private static final String FSA_API_VERSION = "x-api-version";
    private static final Logger LOG = LoggerFactory.getLogger(FsaServiceClient.class);
    private final WebClient webClient;

    public FsaServiceClient(@Value("${app.external.fsa.baseUrl}") final String baseUrl, TcpClient tcpClient) {
        this.webClient = buildWebClient(baseUrl, tcpClient);
    }

    private static Authority toAuthority(FsaAuthorityDto fsaAuthorityDto) {
        return new Authority(fsaAuthorityDto.getLocalAuthorityId(),
                fsaAuthorityDto.getName(), fsaAuthorityDto.getRegion());
    }

    private static List<Authority> toAuthorityList(FsaAuthoritiesDto fsaAuthoritiesDto) {
        return fsaAuthoritiesDto.getAuthorities().stream()
                .map(FsaServiceClient::toAuthority)
                .collect(toList());
    }

    private static List<HygeneRatingLevel> toHygeneRating(FsaEstablishmentsDto fsaEstablishmentsDto) {
        return fsaEstablishmentsDto.getEstablishments().stream()
                .map(FsaEstablishmentDto::getRatingValue)
                .map(HygeneRatingLevel::fromString)
                .collect(toList());
    }

    private static <K> Mono<K> handleClientError(Throwable throwable) {
        var message = String.format("Error connecting to FSA: %s: %s",
                throwable.getClass().getCanonicalName(),
                throwable.getMessage());
        LOG.error(message);
        return Mono.error(new FsaServerException(message, throwable));
    }

    private WebClient buildWebClient(String baseUrl, TcpClient tcpClient) {

        return WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(HttpClient.from(tcpClient)))
                .baseUrl(baseUrl)
                .defaultHeader(FSA_API_VERSION, "2")
                .build();
    }

    @Override
    public Mono<List<Authority>> findAllAuthorities() {

        return webClient.get()
                .uri("/authorities/basic")
                .retrieve()
                .bodyToMono(FsaAuthoritiesDto.class)
                .onErrorResume(FsaServiceClient::handleClientError)
                .map(FsaServiceClient::toAuthorityList);
    }

    Mono<Authority> findAuthorityById(long id) {
        return webClient.get()
                .uri("/authorities/{id}", id)
                .retrieve()
                .bodyToMono(FsaAuthorityDto.class)
                .onErrorResume(FsaServiceClient::handleClientError)
                .map(FsaServiceClient::toAuthority);
    }

    Mono<List<HygeneRatingLevel>> findRatingsForAuthority(long authorityId) {
        return webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/establishments")
                        .queryParam("localAuthorityId", authorityId)
                        .build())
                .retrieve()
                .bodyToMono(FsaEstablishmentsDto.class)
                .onErrorResume(FsaServiceClient::handleClientError)
                .map(FsaServiceClient::toHygeneRating);
    }
}
