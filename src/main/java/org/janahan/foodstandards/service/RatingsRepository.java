package org.janahan.foodstandards.service;

import org.janahan.foodstandards.domain.RatingsSummary;
import reactor.core.publisher.Mono;

public interface RatingsRepository {
    Mono<RatingsSummary> ratingsSummaryForAuthorityAsync(long authorityId);
}
