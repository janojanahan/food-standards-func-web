package org.janahan.foodstandards.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
class FsaEstablishmentsDto {

    private final List<FsaEstablishmentDto> establishments;

    FsaEstablishmentsDto(@JsonProperty("establishments") List<FsaEstablishmentDto> establishments) {
        this.establishments = establishments;
    }

    public List<FsaEstablishmentDto> getEstablishments() {
        return establishments;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class FsaEstablishmentDto {
        private final String ratingValue;

        FsaEstablishmentDto(@JsonProperty("RatingValue") String ratingValue) {
            this.ratingValue = ratingValue;
        }

        public String getRatingValue() {
            return ratingValue;
        }
    }
}
