package org.janahan.foodstandards.service;

import org.janahan.foodstandards.domain.Authority;
import org.janahan.foodstandards.domain.HygeneRatingLevel;
import org.janahan.foodstandards.domain.RatingsSummary;
import org.janahan.foodstandards.domain.RatingsSummary.RatingItem;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;

import static org.janahan.foodstandards.domain.HygeneRatingLevel.EXEMPT;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.FIVE;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.FOUR;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.NEEDS_IMPROVEMENT;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.ONE;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.PASS;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.THREE;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.TWO;

@Component
public class FsaRatingsService implements RatingsRepository {
    private static final EnumSet<HygeneRatingLevel> ENGLISH_RATINGS =
            EnumSet.of(FIVE, FOUR, THREE, TWO, ONE, EXEMPT);

    private static final EnumSet<HygeneRatingLevel> SCOTTISH_RATINGS =
            EnumSet.of(PASS, NEEDS_IMPROVEMENT);
    private final FsaServiceClient fsaServiceClient;

    public FsaRatingsService(FsaServiceClient fsaServiceClient) {
        this.fsaServiceClient = fsaServiceClient;
    }

    @Override
    public Mono<RatingsSummary> ratingsSummaryForAuthorityAsync(final long authorityId) {
        final Mono<Authority> authority = fsaServiceClient.findAuthorityById(authorityId);
        final Mono<List<HygeneRatingLevel>> listOfRatings =
                fsaServiceClient.findRatingsForAuthority(authorityId);

        return authority.zipWith(listOfRatings, this::createSummary);
    }

    private RatingsSummary createSummary(Authority authority, List<HygeneRatingLevel> hygeneRatings) {
        final EnumMap<HygeneRatingLevel, Long> ratingsMap = new EnumMap<>(HygeneRatingLevel.class);
        hygeneRatings.forEach(rating -> ratingsMap.merge(rating, 1L, Long::sum));

        EnumSet<HygeneRatingLevel> referenceSet = authority.isScotland() ? SCOTTISH_RATINGS : ENGLISH_RATINGS;

        // Create list, also adding valid ratings for the region where there are no establishments getting that
        // rating.
        List<RatingItem> ratings = new ArrayList<>();
        referenceSet.forEach(ref -> ratings.add(new RatingItem(ref, ratingsMap.getOrDefault(ref, 0L))));
        return new RatingsSummary(ratings);
    }
}
