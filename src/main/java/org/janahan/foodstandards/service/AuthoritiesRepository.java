package org.janahan.foodstandards.service;

import org.janahan.foodstandards.domain.Authority;
import reactor.core.publisher.Mono;

import java.util.List;

public interface AuthoritiesRepository {
    Mono<List<Authority>> findAllAuthorities();
}
