foodStandardsApp.controller('AppController', function ($scope, $rootScope, ApiService, $mdDialog) {
    $scope.ratingsData = undefined

    var self = this

    this.querySearch = function (query) {
        return ApiService.filterAuthority(query)
    }

    this.selectedItemChange = function (item) {
        if (item) {
            $scope.resultsLoading = true
            ApiService.getRatings(item.idCode)
                .then(
                    function (ratings) {
                        $scope.resultsLoading = undefined
                        $scope.ratingsData = ratings
                    },
                    function () {
                        $scope.resultsLoading = undefined
                        $rootScope.$broadcast(SERVER_ERROR, "Error getting ratings.")
                    }
                )
        } else {
            $scope.ratingsData = undefined
        }
    }
    $rootScope.$on(SERVER_ERROR, function (event, reason) {
        $mdDialog.show(
            $mdDialog.alert()
                .clickOutsideToClose(true)
                .title('Server Error')
                .textContent(reason)
                .ariaLabel('Error')
                .ok('Close')
        );
    })
})