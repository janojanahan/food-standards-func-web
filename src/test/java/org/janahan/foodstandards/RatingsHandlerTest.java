package org.janahan.foodstandards;

import org.janahan.foodstandards.domain.RatingsSummary;
import org.janahan.foodstandards.service.RatingsRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RatingsHandlerTest {
    @Mock
    private RatingsRepository ratingsRepository;

    @Mock
    private ServerRequest serverRequest;

    private RatingsHandler ratingsHandler;

    @Before
    public void setUp() {
        ratingsHandler = new RatingsHandler(ratingsRepository);
    }

    @Test
    public void shouldReturnBodyWhenExpectd() {
        // GIVEN
        var expectedBody = Mono.just(new RatingsSummary(List.of()));
        when(serverRequest.pathVariable(eq("id")))
                .thenReturn("20");
        when(ratingsRepository.ratingsSummaryForAuthorityAsync(20))
                .thenReturn(expectedBody);


        // WHEN
        var result = ratingsHandler.handleRatings(serverRequest);

        // THEN
        assertThat(result.block().statusCode())
                .isEqualTo(HttpStatus.OK);
        verify(serverRequest).pathVariable(eq("id"));
        verify(ratingsRepository).ratingsSummaryForAuthorityAsync(20);
        verifyNoMoreInteractions(serverRequest, ratingsRepository);
    }

    @Test
    public void shouldReturnBadRequestOnBadId() {
        // GIVEN
        when(serverRequest.pathVariable(eq("id")))
                .thenReturn("bad");


        // WHEN
        var result = ratingsHandler.handleRatings(serverRequest);

        // THEN
        assertThat(result.block().statusCode())
                .isEqualTo(HttpStatus.BAD_REQUEST);
        verify(serverRequest).pathVariable(eq("id"));
        verifyNoMoreInteractions(serverRequest, ratingsRepository);
    }
}