package org.janahan.foodstandards.service;

import org.janahan.foodstandards.domain.Authority;
import org.janahan.foodstandards.domain.HygeneRatingLevel;
import org.janahan.foodstandards.domain.RatingsSummary;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.EXEMPT;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.FIVE;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.FOUR;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.NEEDS_IMPROVEMENT;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.ONE;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.PASS;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.THREE;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.TWO;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FsaRatingsServiceTest {

    @Mock
    private FsaServiceClient fsaServiceClient;

    private FsaRatingsService fsaRatingsService;

    @Before
    public void setup() {
        fsaRatingsService = new FsaRatingsService(fsaServiceClient);
    }

    @Test
    public void shouldCreateEnglishSummary() {

        //Given
        long authorityId = 123L;
        final List<HygeneRatingLevel> listOfRatings = Arrays.asList(ONE, ONE, EXEMPT, TWO, FIVE, TWO, ONE, FIVE, THREE);
        when(fsaServiceClient.findRatingsForAuthority(authorityId))
                .thenReturn(Mono.just(listOfRatings));
        when(fsaServiceClient.findAuthorityById(authorityId))
                .thenReturn(Mono.just(new Authority(authorityId, "test", "London")));

        //When
        final RatingsSummary summary = fsaRatingsService.ratingsSummaryForAuthorityAsync(authorityId).block();


        //Then
        assertThat(summary.getRatingsList())
                .hasSize(6).extracting("hygeneRatingLevel", "count")
                .containsExactly(
                        tuple(FIVE, 2L),
                        tuple(FOUR, 0L),
                        tuple(THREE, 1L),
                        tuple(TWO, 2L),
                        tuple(ONE, 3L),
                        tuple(EXEMPT, 1L)
                );
        assertThat(summary.getTotalEstablishments()).isEqualTo(9L);
        verify(fsaServiceClient).findRatingsForAuthority(authorityId);
        verify(fsaServiceClient).findAuthorityById(authorityId);
    }

    @Test
    public void shouldCreateScottishSummary() {
        //Given
        long authorityId = 512L;
        final List<HygeneRatingLevel> listOfRatings = Arrays.asList(PASS, NEEDS_IMPROVEMENT, PASS,
                NEEDS_IMPROVEMENT, PASS);
        when(fsaServiceClient.findRatingsForAuthority(authorityId))
                .thenReturn(Mono.just(listOfRatings));
        when(fsaServiceClient.findAuthorityById(authorityId))
                .thenReturn(Mono.just(new Authority(authorityId, "test", "Scotland")));

        //When
        final RatingsSummary summary = fsaRatingsService.ratingsSummaryForAuthorityAsync(authorityId).block();


        //Then
        assertThat(summary.getRatingsList())
                .hasSize(2).extracting("hygeneRatingLevel", "count")
                .containsExactly(
                        tuple(PASS, 3L),
                        tuple(NEEDS_IMPROVEMENT, 2L)
                );
        assertThat(summary.getTotalEstablishments()).isEqualTo(5L);
        verify(fsaServiceClient).findRatingsForAuthority(authorityId);
        verify(fsaServiceClient).findAuthorityById(authorityId);
    }

    @Test
    public void shouldComputeAsyncOnlyWhenExternalCallsComplete() {

        //Given
        long authorityId = 123L;

        when(fsaServiceClient.findRatingsForAuthority(authorityId))
                .thenReturn(Mono.never());
        when(fsaServiceClient.findAuthorityById(authorityId))
                .thenReturn(Mono.never());

        //When
        var summary =
                fsaRatingsService.ratingsSummaryForAuthorityAsync(authorityId);


        //Then
        try {
            summary.block(Duration.of(2, ChronoUnit.SECONDS));
        } catch (RuntimeException e) {
            assertThat(e.getMessage())
                    .isEqualToIgnoringCase("Timeout on blocking read for 2000 MILLISECONDS");
        }
        verify(fsaServiceClient).findRatingsForAuthority(authorityId);
        verify(fsaServiceClient).findAuthorityById(authorityId);
    }

}