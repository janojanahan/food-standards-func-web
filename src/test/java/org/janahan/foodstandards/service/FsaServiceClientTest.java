package org.janahan.foodstandards.service;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.janahan.foodstandards.domain.Authority;
import org.janahan.foodstandards.domain.HygeneRatingLevel;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.util.StreamUtils;
import reactor.core.publisher.Mono;
import reactor.netty.tcp.TcpClient;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.EXEMPT;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.FIVE;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.FOUR;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.ONE;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.THREE;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.TWO;

public class FsaServiceClientTest {

    private static final Integer TEST_REMOTE_TIMEOUT = 1000;
    @Rule
    public final WireMockRule wireMockRule = new WireMockRule(8090);

    private static final String FSA_BASE_URL = "http://localhost";

    private FsaServiceClient fsaServiceClient;


    @Before
    public void setUp() {
        String baseUrl = FSA_BASE_URL + ':' + wireMockRule.port();
        var tcpclient = TcpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, TEST_REMOTE_TIMEOUT)
                .doOnConnected(connection ->
                        connection.addHandlerLast(new ReadTimeoutHandler(TEST_REMOTE_TIMEOUT, TimeUnit.MILLISECONDS))
                                .addHandlerLast(new WriteTimeoutHandler(TEST_REMOTE_TIMEOUT, TimeUnit.MILLISECONDS)
                                ));
        fsaServiceClient = new FsaServiceClient(baseUrl, tcpclient);
    }

    @Test
    public void shouldGetListOfAuthorities() throws IOException {
        // Given
        String givenJsonResponse = StreamUtils.copyToString(
                this.getClass()
                        .getResourceAsStream("/connector_data/basic_authorities_test.json"), Charset.defaultCharset());

        stubFor(get(urlEqualTo("/authorities/basic"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(givenJsonResponse)));

        // When
        List<Authority> authoritiesList = fsaServiceClient.findAllAuthorities().block();

        // Then
        assertThat(authoritiesList)
                .hasSize(14)
                .extracting("name")
                .containsExactly(
                        "Aberdeen City",
                        "Aberdeenshire",
                        "Adur",
                        "Allerdale",
                        "Amber Valley",
                        "Anglesey",
                        "Angus",
                        "Antrim and Newtownabbey",
                        "Ards and North Down",
                        "Argyll and Bute",
                        "Armagh City, Banbridge and Craigavon",
                        "Arun",
                        "Ashfield",
                        "Ashford");
        assertThat(authoritiesList)
                .extracting("idCode")
                .containsExactly(
                        197L,
                        198L,
                        277L,
                        158L,
                        48L,
                        334L,
                        199L,
                        132L,
                        133L,
                        200L,
                        134L,
                        278L,
                        77L,
                        249L);
    }

    @Test
    public void shouldGetWestminsterAuthorityById() throws IOException {
        // Given
        String givenJsonResponse = StreamUtils.copyToString(
                this.getClass().getResourceAsStream(
                        "/connector_data/westminster_authority.json"), Charset.defaultCharset());

        stubFor(get(urlEqualTo("/authorities/120"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(givenJsonResponse)));

        // When
        Authority authority = fsaServiceClient.findAuthorityById(120).block();

        // Then
        assertThat(authority.getName())
                .isEqualTo("Westminster");
        assertThat(authority.isScotland())
                .isFalse();
    }

    @Test
    public void shouldGetAberdeenAuthorityById() throws IOException {
        // Given
        String givenJsonResponse = StreamUtils.copyToString(
                this.getClass().getResourceAsStream(
                        "/connector_data/aberdeen_authority.json"), Charset.defaultCharset());

        stubFor(get(urlEqualTo("/authorities/197"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(givenJsonResponse)));

        // When
        Authority authority = fsaServiceClient.findAuthorityById(197).block();

        // Then
        assertThat(authority.getName())
                .isEqualTo("Aberdeen City");
        assertThat(authority.isScotland())
                .isTrue();
    }

    @Test
    public void shouldCompleteExceptionallyOnServerErrorWhenGettingAuthorities() {
        // Given

        stubFor(get(urlEqualTo("/authorities/basic"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(500)
                        .withHeader("Content-Type", "application/json")
                        .withBody("")));


        // When
        Mono<List<Authority>> monoAuthoritiesList = fsaServiceClient.findAllAuthorities();


        // Then
        try {
            monoAuthoritiesList.block();
            fail("Expected FsaServerException");
        } catch (FsaServerException fsaServerException) {
            assertThat(fsaServerException.getMessage())
                    .isEqualToIgnoringCase("Error connecting to FSA: " +
                            "org.springframework.web.reactive.function.client.WebClientResponseException.InternalServerError: " +
                            "500 Internal Server Error");
        }

    }

    @Test
    public void shouldhandleConnectionRefused() {
        // Given

        wireMockRule.stop();


        // When
        Mono<List<Authority>> monoAuthoritiesList = fsaServiceClient.findAllAuthorities();


        // Then
        try {
            monoAuthoritiesList.block();
            fail("Expected FsaServerException");
        } catch (FsaServerException fsaServerException) {
            assertThat(fsaServerException.getMessage())
                    .startsWith("Error connecting to FSA: io.netty.channel.AbstractChannel.AnnotatedConnectException: Connection refused:");
        }

    }

    @Test
    public void shouldhandleConnectionTimeout() throws IOException {
        // Given
        String givenJsonResponse = StreamUtils.copyToString(
                this.getClass()
                        .getResourceAsStream("/connector_data/basic_authorities_test.json"), Charset.defaultCharset());

        stubFor(get(urlEqualTo("/authorities/basic"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withFixedDelay(1500)
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(givenJsonResponse)));


        // When
        Mono<List<Authority>> monoAuthoritiesList = fsaServiceClient.findAllAuthorities();


        // Then
        try {
            monoAuthoritiesList.block();
            fail("Expected FsaServerException");
        } catch (FsaServerException fsaServerException) {
            assertThat(fsaServerException.getMessage())
                    .startsWith("Error connecting to FSA: io.netty.handler.timeout.ReadTimeoutException:");
        }

    }

    @Test
    public void shouldGetListOfRatingsForAuthority() throws IOException {
        // Given
        String givenJsonResponse = StreamUtils.copyToString(
                this.getClass()
                        .getResourceAsStream("/connector_data/establishments_test.json"), Charset.defaultCharset());


        stubFor(get(urlEqualTo("/establishments?localAuthorityId=84"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(givenJsonResponse)));

        // When
        List<HygeneRatingLevel> hygeneRatingList = fsaServiceClient.findRatingsForAuthority(84L).block();

        // Then
        assertThat(hygeneRatingList)
                .hasSize(11)
                .containsExactly(FIVE, FIVE, FOUR, FOUR, TWO, ONE, EXEMPT, THREE, FOUR, FIVE, FIVE);

    }


    @Test
    public void shouldCompleteExceptionallyOnServerErrorWhenGettingEstablishments() {
        // Given

        stubFor(get(urlEqualTo("/establishments?localAuthorityId=234"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.SERVICE_UNAVAILABLE.value())
                        .withHeader("Content-Type", "application/json")
                        .withBody("")));


        // When
        var monoAuthoritiesList =
                fsaServiceClient.findRatingsForAuthority(234L);


        // Then
        try {
            monoAuthoritiesList.block();
            fail("Expected FsaServerException");
        } catch (FsaServerException fsaServerException) {
            assertThat(fsaServerException.getMessage())
                    .isEqualToIgnoringCase("Error connecting to FSA: " +
                            "org.springframework.web.reactive.function.client.WebClientResponseException.ServiceUnavailable: " +
                            "503 Service Unavailable");
        }
    }
}